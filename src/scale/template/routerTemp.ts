import utilTool from "../../util/tool"
import { IJson } from "../../interface/type"

export function routerTemp(pageConfig: IJson) {
  const upServerName = utilTool.lineToUp(pageConfig.list.fileName)
  const editName = utilTool.lineToUp(pageConfig.modify.fileName)
  const routerFile = utilTool.firstLower(utilTool.lineToUp(pageConfig.list.fileName))
  console.log('pageConfig.list?.name> ', pageConfig.list?.name)
  let sName = pageConfig.list?.name?.replace(/管理/ig, "")
  sName = sName?.replace(/列表/ig, "")

  return `
    const ${upServerName} = resolve => require.ensure([], () => resolve(require('@/components/${pageConfig.filePath}/${pageConfig.list.fileName}')), '${pageConfig.chunkName}')
    const ${editName} = resolve => require.ensure([], () => resolve(require('@/components/${pageConfig.filePath}/${pageConfig.modify.fileName}')), '${pageConfig.chunkName}')
    export const ${routerFile}Router = [{
      path: '/${pageConfig.list.fileName}',
      name: '${upServerName}',
      component: ${upServerName},
      meta: {
        keepAlive: true,
        pageName: '${pageConfig.list.name}'
      }
    }, {
      path: '/${pageConfig.modify.fileName}/:type/:id',
      name: '${editName}',
      component: ${editName},
      meta: {
        keepAlive: false,
        pageName: {
          /*
          * tab栏显示的名称 支持多值传输
          * */
          add: '新建${sName}',
          details: '${sName}详情',
          edit: '${sName}编辑'
        }
      }
    }]
  `
}
