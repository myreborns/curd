import { vbenConfig } from "../../vbenConfig"
import utilTool from "../../util/tool"
import { IResAndReqFieldsMap, IJson } from "../../interface/type"
import { apiTemp } from "../template/apiTemp"
import { SwResult } from "../../store/swResult"

export async function createApiFile(sw: SwResult, apiPramAndRes: IResAndReqFieldsMap, pageConfig: IJson) {
  // 指定要创建的目录和文件名称 __dirname为执行当前js文件的目录
  let fileName = pageConfig.filePath ? pageConfig.filePath : vbenConfig.baseUrl.replace(/\//g, '')
  fileName = utilTool.firstLower(utilTool.lineToUp(fileName))
  const filePath = await utilTool.checkAndCreatePath(`/src/api/${vbenConfig.modelName}`)
  utilTool.generateFile(filePath + `/${fileName}.ts`, apiTemp(sw, apiPramAndRes, pageConfig))
}

