import { IMethod, ISingleInterface } from "../interface/type"
export class Server {
  private interfaceMap: Map<string, ISingleInterface>;

  // server下面方法
  private methodMap: Map<string, IMethod>;

  constructor() {
    this.interfaceMap = new Map<string, ISingleInterface>();
    this.methodMap = new Map<string, IMethod>();
  }

  // 设置方法相关对象
  setInterface(methodName: string, methodObj: ISingleInterface) {
    this.interfaceMap.set(methodName, methodObj)
  }

  // 获取方法相关对象
  getInterface(methodName: string): ISingleInterface | undefined {
    return this.interfaceMap.get(methodName)
  }

  // 获取server结果
  getInterfaceMap(): Map<string, ISingleInterface> {
    return this.interfaceMap
  }

  // 设置方法相关对象
  setMethod(methodName: string, methodObj: IMethod) {
    this.methodMap.set(methodName, methodObj)
  }

  // 获取方法相关对象
  getMethod(methodName: string): IMethod | undefined {
    return this.methodMap.get(methodName)
  }

  // 获取server结果
  geMethodMap(): Map<string, IMethod> {
    return this.methodMap
  }

}
