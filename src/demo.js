const puppeteer = require('puppeteer');
const log = console.log;
// const iPhone = puppeteer.devices['iPhone 6'];
(async () => {
  // 创建一个浏览器对象
  const browser = await puppeteer.launch({
    // headless: false,   //有浏览器界面启动
    // slowMo: 100,       //放慢浏览器执行速度，方便测试观察
    // args: [            //启动 Chrome 的参数，详见上文中的介绍
    //   '–no-sandbox',
    //   '--window-size=1280,960'
    // ],
  })
  log('服务启动')

  // 打开一个新的页面
  const page = await browser.newPage()
  // 模拟不同的设备
  // await page.emulate(iPhone);

  // 监听页面内部的console消息
  // page.on('console', msg => {
  //   if (typeof msg === 'object') {
  //     console.dir(msg)
  //   } else {
  //     log(msg)
  //   }
  // })

  let titleList = []

  const handleData = async () => {
    // 通过 page.evaluate 在浏览器里执行
    titleList = await page.evaluate(() => {
      // 此处是在浏览器里执行，所以不能直接获取外部变量，如果直接使用titleList，这是获取不到的
      console.log('在浏览器里执行')
      const lis = document.querySelectorAll('#hotsearch-content-wrapper li')

      let list = []
      lis.forEach(item => {
        list.push(item.querySelector('.title-content-title').textContent)
      })
      return list
    })
  }

  try {
    // 设置页面的URL
    await page.goto('https://www.baidu.com')
    // other actions...
    log('页面初次加载完毕')
    // 获取百度热搜
    const newList = await page.$('#hotsearch-content-wrapper')
    console.dir(newList)

    await handleData()
    log('-----------------------热搜标题-------------------')
    log(titleList.join(','))
  } catch (error) {
    log('--------------error-----------------------')
    console.dir(error)
  } finally {
    // 最后关闭浏览器（如果不关闭，node程序也不会结束的）
    await browser.close()
  }

})()
