import utilTool from "../../util/tool"
import { vbenConfig } from "../../vbenConfig"
import { IJson, IResAndReqFieldsMap } from "../../interface/type"
import { editTemp } from "../template/editTemp"
import { editDataTemp } from "../template/editDataTemp"

export async function createEditFile(pageConfig: IJson, apiPramAndRes: IResAndReqFieldsMap) {
  if (!pageConfig?.modify?.fileName) {
    return
  }
  // 指定要创建的目录和文件名称 __dirname为执行当前js文件的目录
  const basePath = pageConfig.filePath ? pageConfig.filePath : vbenConfig.baseUrl.replace(/\//g, '')
  const fileName = pageConfig.modify?.fileName ? utilTool.firstUp(pageConfig.modify.fileName) : 'Modify'
  const filePath = await utilTool.checkAndCreatePath(`/src/views/${vbenConfig.modelName}/${basePath}`)

  // console.log('filePath: ', filePath, param)
  console.log('pageConfig: ', pageConfig)
  utilTool.generateFile(filePath + `/${pageConfig.modify.fileName}.data.ts`, editDataTemp(apiPramAndRes, pageConfig))
  utilTool.generateFile(filePath + `/${fileName}Drawer.vue`, editTemp(pageConfig))
}
