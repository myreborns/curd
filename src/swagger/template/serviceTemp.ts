import { IMethod } from "../../interface/type"
import { swaggerConfig } from "../../swaggerConfig"
function getServeList(list: IMethod[]): string {
  const interfaces: string[] = []
  for (const item of list) {
    const desc = item.description
    if (desc && desc.indexOf("\n") > 0) {
      interfaces.push(`'${item.name}', // ${desc}`);
    } else {
      interfaces.push(`'${item.name}', // ${desc} \n`);
    }
    // interfaces.push(`'${item.name}', // ${desc} \n`);
  }
  return interfaces.join('')
}

export function serviceTemp(list: IMethod[], serverName: string, basePath: string) {
  return `
    import { serviceFactory } from '../tsBase'
    const baseUrl = '${swaggerConfig.baseUrl}'
    const defaultOptions = {
      gateway: '${basePath}',
    }
    const interfaces = [
      ${getServeList(list)}
    ]
    const ${serverName} = serviceFactory(interfaces, baseUrl, defaultOptions)
    export default ${serverName}
  `
}