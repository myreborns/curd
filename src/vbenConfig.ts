// 适用于swagger api 的配置 的vben api
export const vbenConfig = {
  href: 'http://staging-wealth-openapi.today36524.cn',
  serverName: 'today-wealth-service',
  baseUrl: '/wealth/',
  tagName: '门店管理',
  modelName: 'zqb', // 模块文件夹名称，参考vben-admin　src/api/下的文件
  pages: [{
    filePath: 'store',     // 列表＼新＼修页面所在路径
    list: { // 列表
      fileName: 'index', // 列表文件名字
      name: '门店管理', // 菜单名
      apiName: 'get',  // 列表接口名
      exportFile: { // 导出 [非必填]
        apiName: '',
        downFileName: ''
      }
    },
    modify: { // (修改＼新增)  [非必填]
      fileName: 'modify',  // 页面名
      apiName: 'saveStore',  // 接口名
      detailApi: 'get', // 查看详情接口名 (用于查看详情、编辑)
      hasEdit: true
    },
    // opts: [ // 表格行操作
    //   {
    //     apiName: 'deleteStore', // 接口
    //     confrimMsg: '是否确认删除门店', // 如果没有确认信息，则直接请求
    //     text: '删除'
    //   },
    //   {
    //     apiName: 'openStore', // 接口
    //     confrimMsg: '是否确认门店再营业', // 如果没有确认信息，则直接请求
    //     text: '再营业'
    //   },
    //   {
    //     apiName: 'closeStore', // 接口
    //     confrimMsg: '是否确认门店闭店', // 如果没有确认信息，则直接请求
    //     text: '闭店'
    //   }
    // ]
  }]
}

