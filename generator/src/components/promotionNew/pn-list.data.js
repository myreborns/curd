import adminStoreContractServiceEnums from '@/assets/js/enum/co-admin-store-contract-service-enums'
import util from '@/assets/js/co-util'
export const tableConfig = {
	showIndex: true, // 显示序号
	listResultName: 'storeContractList', // 这里是列表接口返回表格数据的key，能让后台统一最好，不能统一也需要有一个规则，否则生成后需要改
	searchForm: {
		baseColProps: { span: 8 },
		// showExportButton: true,
		// hasExportPermission: () => { // 是否有导出的权限
		//   return this.hasPermission('FUN_1_')
		// },
		schemas: [
			{
				field: 'storeCode',
				label: '门店物理店号:',
				component: 'el-select',
				componentProps: {
					options: [],
				},

				dynamicRules: [{ required: true, message: '请选择门店物理店号', trigger: ['blur', 'change'] }],
			},
		],
	},
	columns: [
		{
			prop: 'id',
			label: '主键ID',
			// width: '110px',
		},
		{
			prop: 'storeCode',
			label: '门店号',
			// width: '110px',
		},
		{
			prop: 'storeId',
			label: '财务店号',
			// width: '110px',
		},
		{
			prop: 'contractNo',
			label: '合同编号',
			// width: '110px',
		},
		{
			prop: 'contractStatus',
			label: '状态', // 生效中(normal);2
			// width: '110px',
			formatter: (row, cloumn, value) => {
				return util.getEnumLabel(adminStoreContractServiceEnums.ContractStatusEnum, value)
			},
		},
		{
			prop: 'joinPlan',
			label: '加盟方案', // A方案(a);2
			// width: '110px',
			formatter: (row, cloumn, value) => {
				return util.getEnumLabel(adminStoreContractServiceEnums.JoinPlanEnum, value)
			},
		},
		{
			prop: 'pointsPlan',
			label: '提点方案', // 毛利额(gross);2
			// width: '110px',
			formatter: (row, cloumn, value) => {
				return util.getEnumLabel(adminStoreContractServiceEnums.PointsPlanEnum, value)
			},
		},
		{
			prop: 'pointsRatio',
			label: '提点比例',
			// width: '110px',
		},
		{
			prop: 'openDate',
			label: '开业时间',
			// width: '110px',
			// formatter: (row, cloumn, value) => { return value ? util.formatShortDate(value, 'YYYY-MM-DD hh:mm:ss') : '--' }
			formatter: (row, cloumn, value) => {
				return value ? util.formatDate(value) : '--'
			},
		},
		{
			prop: 'breakEvenMonth',
			label: '保本周期',
			// width: '110px',
		},
		{
			prop: 'breakEvenClauseList',
			label: '保本条款',
			// width: '110px',
		},
		{
			prop: 'partner',
			label: '加盟商',
			// width: '110px',
		},
		{
			prop: 'largeCustomer',
			label: '是否为大客户', // 不是(no);1
			// width: '110px',
			formatter: (row, cloumn, value) => {
				return util.getEnumLabel(adminStoreContractServiceEnums.LargeCustomerEnum, value)
			},
		},
		{
			prop: 'attachmentList.name',
			label: '附件名称',
			// width: '110px',
		},
		{
			prop: 'attachmentList.url',
			label: '附件地址',
			// width: '110px',
		},
		{
			prop: 'remark',
			label: '备注',
			// width: '110px',
		},
		{
			prop: 'createdAt',
			label: '创建时间',
			// width: '110px',
			// formatter: (row, cloumn, value) => { return value ? util.formatShortDate(value, 'YYYY-MM-DD hh:mm:ss') : '--' }
			formatter: (row, cloumn, value) => {
				return value ? util.formatDate(value) : '--'
			},
		},
		{
			prop: 'createdBy',
			label: '创建人',
			// width: '110px',
		},
		{
			prop: 'updatedAt',
			label: '更新时间',
			// width: '110px',
			// formatter: (row, cloumn, value) => { return value ? util.formatShortDate(value, 'YYYY-MM-DD hh:mm:ss') : '--' }
			formatter: (row, cloumn, value) => {
				return value ? util.formatDate(value) : '--'
			},
		},
		{
			prop: 'updatedBy',
			label: '更新人',
			// width: '110px',
		},
		{
			prop: 'updatedByName',
			label: '更新人名称',
			// width: '110px',
		},
		{
			prop: 'storeName',
			label: '门店名称',
			// width: '110px',
		},
		{
			prop: 'storeType',
			label: '门店类型',
			// width: '110px',
			formatter: (row, cloumn, value) => {
				return util.getEnumLabel(adminStoreContractServiceEnums.StoreTypeEnum, value)
			},
		},
		{
			prop: 'cityName',
			label: '门店城市',
			// width: '110px',
		},
		{
			prop: 'areaName',
			label: '门店区域',
			// width: '110px',
		},
	],
}
