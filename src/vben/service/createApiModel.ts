import { vbenConfig } from "../../vbenConfig"
import utilTool from "../../util/tool"
import { IResAndReqFieldsMap, IJson } from "../../interface/type"
import { apiModelTemp } from "../template/apiModelTemp"

export async function createApiModelFile(apiPramAndResMap: IResAndReqFieldsMap, pageConfig: IJson) {
  // 指定要创建的目录和文件名称 __dirname为执行当前js文件的目录
  const filePath = await utilTool.checkAndCreatePath(`/src/api/${vbenConfig.modelName}/model`)
  const basePath = pageConfig.filePath ? pageConfig.filePath : vbenConfig.baseUrl.replace(/\//g, '')
  const fileName = utilTool.firstLower(utilTool.lineToUp(`${basePath}Model.ts`))
  utilTool.generateFile(filePath + `/${fileName}`, apiModelTemp(apiPramAndResMap))
}

