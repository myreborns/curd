import utilTool from "../../util/tool"
import { vbenConfig } from "../../vbenConfig"
import { IJson, IResAndReqFieldsMap } from "../../interface/type"
import { listTemp } from "../template/listTemp"
import { listDataTemp } from "../template/listDataTemp"

export async function createListFile(pageConfig: IJson, apiPramAndRes: IResAndReqFieldsMap) {
  if (!pageConfig?.list?.apiName && !pageConfig?.modify?.apiName) {
    return
  }
  // 指定要创建的目录和文件名称 __dirname为执行当前js文件的目录
  const basePath = pageConfig.filePath ? pageConfig.filePath : vbenConfig.baseUrl.replace(/\//g, '')
  const fileName = pageConfig.list?.fileName ? pageConfig.list.fileName : 'index'
  const filePath = await utilTool.checkAndCreatePath(`/src/views/${vbenConfig.modelName}/${basePath}`)

  // console.log('filePath: ', filePath, param)
  console.log('pageConfig: ', pageConfig)
  utilTool.generateFile(filePath + `/${fileName}.data.ts`, listDataTemp(apiPramAndRes, pageConfig))
  utilTool.generateFile(filePath + `/${fileName}.vue`, listTemp(apiPramAndRes, pageConfig))
}
