import utilTool from "../../util/tool"
import { IJson } from "../../interface/type"
import { editDataTemp } from "../template/editDataTemp"
import { editTemp } from "../template/editTemp"
import { IField } from "../../interface/type"

export async function createEditFile(pageConfig: IJson, request: IField[]) {
  // 指定要创建的目录和文件名称 __dirname为执行当前js文件的目录
  console.log('----------createEditFile--------------')
  const filePath = await utilTool.checkAndCreatePath(`/src/components/${pageConfig.filePath}`)
  utilTool.generateFile(filePath + `/${pageConfig.modify.fileName}.data.js`, editDataTemp(request, pageConfig))
  utilTool.generateFile(filePath + `/${pageConfig.modify.fileName}.vue`, editTemp(request))
}
