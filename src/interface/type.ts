export interface IField {
  name: string; // 名称
  type?: any; // 类型
  isReq?: boolean; // 是否必填
  description?: string; // 描述
  isEnum?: boolean; // 是否是枚举
  EnumOptionName?: string; // 枚举的名称
  EnumOptions?: IEnum[]; // 枚举值
  children?: IField[] | undefined | null
  component?: string; // 组件名
  compProps?: string; // 组件属性
  format?: string; // 格式化
  childrenFormat?: string; // 子类型格式化
  childrenType?: string; // 子类型
  childrenRef?: string; // 子类对应(有来去重)
}

export interface IResAndReqFields {
  request: IField[]; // 请求参数
  response: IField[]; // 请求结果
  responseType?: string; // 类型
}

export interface IEnum {
  name?: string; // 名称
  value?: any; // 值
}

export interface IEywaEnum {
  value?: any; // 值
  lable?: string // 名称
}

export interface IEnumObj {
  enums?: IEnum[]; // 枚举
  description?: string; // 描述
}

export interface ISingleInterface {
  params?: IField[]; // 请求参数
  result?: IField[]; // 结果
  enums?: IEnum[]; // 枚举
}

export interface IMethod {
  name?: string | null,
  link?: string | null,
  description?: string | null
}

export interface IListConfig { // 列表
  fileName?: string | null, // 列表文件名字
  name?: string | null,  // 菜单名
  apiName?: string | null,  // 列表接口名
  exportFile?: { // 导出 [非必填]
    apiUrl?: string | null,
    downFileName?: string | null
  },
  detailApi?: string, // 查看详情接口名 (用于查看详情、编辑)
  hasEdit?: boolean
}

export interface IJson {
  [key:string]: any
}

export interface IResAndReqFieldsMap {
  [key: string]: IResAndReqFields
}
