import adminStoreContractServiceEnums from '@/assets/js/enum/co-admin-store-contract-service-enums'
import util from '@/assets/js/co-util'
export const formSchemas = [
	{
		field: 'id',
		label: '主键ID:',
		component: 'el-select',
		componentProps: {
			options: [],
		},

		dynamicRules: [{ required: true, message: '请选择主键ID', trigger: ['blur', 'change'] }],
	},
	{
		field: 'storeCode',
		label: '门店店号:',
		component: 'el-select',
		componentProps: {
			options: [],
		},

		dynamicRules: [{ required: true, message: '请选择门店店号', trigger: ['blur', 'change'] }],
	},
	{
		field: 'storeId',
		label: '财务店号:',
		component: 'el-input',
		componentProps: {
			placeholder: '请输入财务店号',
			maxLength: 50, // 可以设置默认最大长度
		},
		dynamicRules: [{ required: true, message: '请输入财务店号', trigger: ['blur', 'change'] }],
	},
	{
		field: 'contractNo',
		label: '合同编号:',
		component: 'el-input',
		componentProps: {
			placeholder: '请输入合同编号',
			maxLength: 50, // 可以设置默认最大长度
		},
		dynamicRules: [{ required: true, message: '请输入合同编号', trigger: ['blur', 'change'] }],
	},
	{
		field: 'joinPlan',
		label: '加盟方案:',
		component: 'el-input',
		componentProps: {
			placeholder: '请输入加盟方案',
			maxLength: 50, // 可以设置默认最大长度
		},
		dynamicRules: [{ required: true, message: '请输入加盟方案', trigger: ['blur', 'change'] }],
	},
	{
		field: 'pointsPlan',
		label: '提点方案:',
		component: 'el-input',
		componentProps: {
			placeholder: '请输入提点方案',
			maxLength: 50, // 可以设置默认最大长度
		},
		dynamicRules: [{ required: true, message: '请输入提点方案', trigger: ['blur', 'change'] }],
	},
	{
		field: 'pointsRatio',
		label: '提点比例:',
		component: 'el-input-number',

		dynamicRules: [{ required: true, message: '请输入提点比例', trigger: ['blur', 'change'] }],
	},
	{
		field: 'openDate',
		label: '开业时间:',
		component: 'el-select',
		componentProps: {
			options: [],
		},

		dynamicRules: [{ required: true, message: '请选择开业时间', trigger: ['blur', 'change'] }],
	},
	{
		field: 'breakEvenMonth',
		label: '保本周期:',
		component: 'el-select',
		componentProps: {
			options: [],
		},

		dynamicRules: [{ required: true, message: '请选择保本周期', trigger: ['blur', 'change'] }],
	},
	{
		field: 'breakEvenClauseList',
		label: '保本条款:',
		component: 'el-input',
		componentProps: {
			placeholder: '请输入保本条款',
			maxLength: 50, // 可以设置默认最大长度
		},
		dynamicRules: [{ required: true, message: '请输入保本条款', trigger: ['blur', 'change'] }],
	},
	{
		field: 'partner',
		label: '加盟商:',
		component: 'el-input',
		componentProps: {
			placeholder: '请输入加盟商',
			maxLength: 50, // 可以设置默认最大长度
		},
		dynamicRules: [{ required: true, message: '请输入加盟商', trigger: ['blur', 'change'] }],
	},
	{
		field: 'largeCustomer',
		label: '是否为大客户:',
		component: 'el-input',
		componentProps: {
			placeholder: '请输入是否为大客户',
			maxLength: 50, // 可以设置默认最大长度
		},
		dynamicRules: [{ required: true, message: '请输入是否为大客户', trigger: ['blur', 'change'] }],
	},
	{
		field: 'remark',
		label: '备注:',
		component: 'el-input',
		componentProps: {
			placeholder: '请输入备注',
			maxLength: 50, // 可以设置默认最大长度
		},
		dynamicRules: [{ required: true, message: '请输入备注', trigger: ['blur', 'change'] }],
	},
	{
		field: 'contractStatus',
		label: '合同状态:',
		component: 'el-select',
		componentProps: {
			options: util.getEnumValues(adminStoreContractServiceEnums.ContractStatusEnum, true),
		},
	},
]
export const attachmentListSchemas = [
	{
		field: 'name',
		label: '附件名称:',
		component: 'el-input',
		componentProps: {
			placeholder: '请输入附件名称',
			maxLength: 50, // 可以设置默认最大长度
		},
		dynamicRules: [{ required: true, message: '请输入附件名称', trigger: ['blur', 'change'] }],
	},
	{
		field: 'url',
		label: '附件地址:',
		component: 'el-input',
		componentProps: {
			placeholder: '请输入附件地址',
			maxLength: 50, // 可以设置默认最大长度
		},
		dynamicRules: [{ required: true, message: '请输入附件地址', trigger: ['blur', 'change'] }],
	},
]
