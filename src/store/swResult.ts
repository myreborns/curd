import { IJson } from "../interface/type"
export class SwResult {
  private definitions = {};
  private paths = {};
  private basePath = '' // 服务根路径

  getDefinitions(): IJson {
    return this.definitions;
  }

  getPaths(): IJson {
    return this.paths;
  }

  setDefinitions(definitions: IJson): void {
    this.definitions = definitions
  }

  setPaths(paths: IJson): void {
    this.paths = paths
  }

  setBasePath(basePath: string) {
    this.basePath = basePath
  }

  getBasePath(): string {
    return this.basePath;
  }

}