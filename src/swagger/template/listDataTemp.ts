import { IJson } from "../../interface/type"
import utilTool from "../../util/tool"
import { swaggerConfig } from '../../swaggerConfig'


function getSchema(children: IJson, parentKey: string): string[] {
  let arr: string[] = []
  if (parentKey) {
    parentKey = parentKey + '.'
  }
  for (const key of Object.keys(children)) {
    const item = children[key]
    if (typeof item !== "object" || key === 'pageRequest') {
      continue
    }
    const currentKey = parentKey + key
    if (children[key] && Object.prototype.hasOwnProperty.call(children[key], 'children')) {
      arr = arr.concat(getSchema(children[key].children, currentKey))
    } else if (children[key]) {
      const upServerName = utilTool.firstLower(utilTool.lineToUp(swaggerConfig.serverName))
      utilTool.updateCompProp(item, key, upServerName)
      const descArr = utilTool.getDescription(item?.description)
      arr.push(`
      {
        field: '${currentKey}',
        label: '${descArr[0]}:',${descArr[1]}
        component: '${item.component}',
        ${item.compProps}
      },`)
    }
  }
  return arr
}

// 根据请求参参数生成搜索条件配置
function getSchemas(json: IJson): string {
  if (!json?.parameters) {
    return ''
  }
  const cjson = JSON.parse(JSON.stringify(json.parameters))
  return getSchema(cjson, '').join('')
}

// 根据字段设置格式化
function getFormatterStr(children: IJson, key: string, lowServerName: string): string {
  let formatterStr = ''
  if (Object.prototype.hasOwnProperty.call(children[key], 'enum')) {
    const enumKey = utilTool.firstUp(key) + 'Enum'
    formatterStr = `formatter: (row, cloumn, value) => {
          return util.getEnumLabel(${lowServerName}Enum.${enumKey}, value)
        },`
  } else if ((children[key].type.toLowerCase() === 'long' || (children[key].type === 'integer' && children[key].format === 'int64')) && utilTool.checkStrIncludeKeyWord(children[key].description, ['时间'])) {
    // 如果是时间
    formatterStr = `// formatter: (row, cloumn, value) => { return value ? util.formatShortDate(value, 'YYYY-MM-DD hh:mm:ss') : '--' }
        formatter: (row, cloumn, value) => { return value ? util.formatDate(value, 'YYYY-MM-DD hh:mm:ss') : '--' },
        width: '160px',`
  } else if ((children[key].type.toLowerCase() === 'long' || (children[key].type === 'integer' && children[key].format === 'int64')) && utilTool.checkStrIncludeKeyWord(children[key].description, ['日'])) {
    // 如果是日期
    formatterStr = `// formatter: (row, cloumn, value) => { return value ? util.formatShortDate(value, 'YYYY-MM-DD hh:mm:ss') : '--' }
        formatter: (row, cloumn, value) => { return value ? util.formatDate(value) : '--' },
        width: '160px',`
  } else if (utilTool.checkStrIncludeKeyWord(children[key].description, ['金额'])) {
    // 如果是金额就格式化一下显示为两位小数
    formatterStr = `formatter: (row, cloumn, value) => {
      return util.formatterNumber(value)
    },`
  }
  return formatterStr
}

function getCol(children: IJson, parentKey: string, lowServerName: string): string[] {
  let arr:string[] = []
  if (parentKey) {
    parentKey = parentKey + '.'
  }
  for (const key of Object.keys(children)) {
    const item = children[key]
    if (typeof item !== "object" || (item.type === 'array' && item.items)) {
      // 排除非对象，以及排除值为数组对象
      continue
    }
    const currentKey = parentKey + key
    if (item && Object.prototype.hasOwnProperty.call(item, 'children')) {
      arr = arr.concat(getCol(item.children, currentKey, lowServerName))
    } else if (item) {
      const formatterStr = getFormatterStr(children, key, lowServerName)
      const descArr = utilTool.getDescription(item?.description)
      arr.push(`
      {
        prop: '${currentKey}',
        label: '${descArr[0]}',${descArr[1]}
        // width: '110px',
        ${formatterStr}
      },`)
    }
  }
  return arr
}

// 获取表格列
function getCols(json: IJson, tableDatakey: string, lowServerName: string): string {
  if (!tableDatakey || !json?.responses || !json?.responses[tableDatakey]) {
    return ''
  }
  const arr: string[] = getCol(json?.responses[tableDatakey].children, '', lowServerName)
  // const columns = json
  return arr.join('')
}

const PAGE_KEYS = ['current', 'hitCount', 'pages', 'searchCount', 'size', 'total']

// 获取返回值中表格数据的key, 规则：1．看'records' 是否存在，如果存在就返回'records'(这一规则与后台约定)　2.是排除分页面外，取第一个类型为数组的数据
function getRecordsKey(responses: IJson): string {
  if (!responses) {
    console.log('出错了，列表接口返回结果，不符合规则，请检查接口是否正确')
    throw new Error('出错了，列表接口返回结果，不符合规则，请检查接口是否正确');
  }
  if (Object.prototype.hasOwnProperty.call(responses, 'records') && responses.records.type && responses.records.type === 'array') {
    return 'records'
  }

  for (const key of Object.keys(responses)) {
    if (PAGE_KEYS.indexOf(key) === -1 && responses[key].type === 'array') {
      return key
    }
  }
  return 'records'
}

export function listDataTemp(json: IJson, pageConfig: IJson) {
  console.log('开始读取数据，准备生成', pageConfig.list.fileName+'.data.js')
  const enumFileName: string = utilTool.upToLine(`co-${utilTool.firstLower(swaggerConfig.serverName)}Enum`)
  const upServerName = utilTool.lineToUp(swaggerConfig.serverName)
  const lowServerName = utilTool.firstLower(upServerName)
  const tableDatakey = getRecordsKey(json.responses)
  return `import ${lowServerName}Enum from '@/assets/js/enum/${enumFileName}'
import util from '@/assets/js/co-util'
export function tableConfig () {
  return {
    showIndex: true, // 显示序号
    isTsApi: true, // 使用ts api
    listResultName: '${tableDatakey}', // 这里是列表接口返回表格数据的key，能让后台统一最好，不能统一也需要有一个规则，否则生成后需要改
    searchForm: {
      baseColProps: { span: 8 },
      ${pageConfig.list?.exportFile?.apiUrl ? '' : '// '}showExportButton: true,
      // hasExportPermission: () => { // 是否有导出的权限
      //   return this.hasPermission('FUN_1_')
      // },
      schemas: [
        ${getSchemas(json)}
      ]
    },
    columns: [
      ${getCols(json, tableDatakey, lowServerName)}
    ]
  }
}
  `
}
