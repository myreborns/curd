import { IJson, IEnum } from "../../interface/type"
function formatArr(arr: IEnum[]): IJson{
  const res: IJson = {}
  arr?.forEach(it => {
    res[it.value] = it
  })
  return res
}

function getEnumContent(json: IJson) {
  const strArr = []
  for (const key of Object.keys(json)) {
    strArr.push(`
      // ${json[key]?.description}
      ${key}: ${JSON.stringify(formatArr(json[key].enums))}
    `)
  }
  return strArr.join(',')
}

export function enumTemp(json: IJson) {
  return `export default {
    ${getEnumContent(json)}
  }`
}
