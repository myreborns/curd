// 适用于scale的配置
export const config = {
  href: 'http://xxx.cn/',
  serverName: 'AdminStoreContractService',
  pages: [{
    filePath: 'promotionNew',     // 列表＼新＼修页面所在路径
    chunkName: 'base', // chunkName 用于路由中 require.ensure 打包参数
    list: { // 列表
      fileName: 'pn-list', // 列表文件名字
      name: '列表', // 菜单名
      apiName: 'listStoreContract',  // 列表接口名
      exportFile: { // 导出 [非必填]
        apiUrl: '',
        downFileName: ''
      }
    },
    modify: { // (修改＼新增)  [非必填]
      fileName: 'modify',  // 页面名
      apiName: 'saveStoreContract',  // 接口名
      detailApi: 'getStoreContractById', // 查看详情接口名 (用于查看详情、编辑)
      hasEdit: true
    },
    opts: [ // 表格行操作
      {
        apiName: 'modifyStoreContractStatus', // 接口
        confrimMsg: '确认审核该记录', // 如果没有确认信息，则直接请求
        text: '审核'
      }
    ]
  }]
}



