import { swaggerConfig } from "../../swaggerConfig"
import utilTool from "../../util/tool"
import { IMethod } from "../../interface/type"
import { serviceTemp } from "../template/serviceTemp"

export async function createApiFile(list: IMethod[], basePath: string) {
  // 指定要创建的目录和文件名称 __dirname为执行当前js文件的目录
  const filePath = await utilTool.checkAndCreatePath('/src/api/service')
  const serverName = utilTool.lineToUp(swaggerConfig.serverName)
  utilTool.generateFile(filePath + `/${serverName}.js`, serviceTemp(list, serverName, basePath))
}

