import { IField, ISingleInterface } from "../interface/type"
export class Method {

  private currentMethod: ISingleInterface;
  private enumHashMap: Map<string, number>;

  constructor(single: ISingleInterface = {
    params: [], // 请求参数
    result: [], // 结果
    enums: [], // 枚举
  }) {
    this.currentMethod = single ? single : {
      params: [], // 请求参数
      result: [], // 结果
      enums: [], // 枚举
    };
    this.enumHashMap = new Map<string, number>();
  }

  getMethod(): ISingleInterface {          // 函数后(): string 这个的意思是 要求函数返回的类型必须是 string
    return this.currentMethod;
  }

  addParam(param: IField) {
    if (!this.currentMethod.params || this.currentMethod?.params?.length < 1) {
      this.currentMethod.params = [param]
    }
    this.currentMethod.params.push(param)
  }

  addResult(result: IField) {
    if (!this.currentMethod.result || this.currentMethod.result?.length < 1) {
      this.currentMethod.result = [result]
    }
    this.currentMethod.result.push(result)
  }

  // 添加枚举，但要去重
  addEnums(enums: IField) {
    if (this.enumHashMap.has(enums.name)) {
      // 根据枚举名做防重处理
      return
    }
    if (!this.currentMethod.enums || this.currentMethod.enums?.length < 1) {
      this.currentMethod.enums = [enums]
    }
    this.currentMethod.enums.push(enums)
    this.enumHashMap.set(enums.name, 1)
  }
}
