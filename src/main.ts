import puppeteer from "puppeteer";
import { IndexAction } from "./scale/action/index";
import { config } from './config'
// const chalk = require("chalk");
// const puppeteer = require("puppeteer");
// const server = require("./action/index.ts")

// const log = console.log;
const init = async () => {
  const browser = await puppeteer.launch({
    headless: false,   // 有浏览器界面启动
    slowMo: 200,       // 放慢浏览器执行速度，方便测试观察
    args: [            // 启动 Chrome 的参数，详见上文中的介绍
      "–no-sandbox",
      "--window-size=1280,960",
      '--no-sandbox',
      '--disable-setuid-sandbox',
      '--disable-dev-shm-usage',
      '--disable-accelerated-2d-canvas',
      '--disable-gpu',
    ],
  });
  // log(chalk.green("服务正常启动"))
  try {
    const page = await browser.newPage();
    const indexAction = new IndexAction(browser, page);
    await indexAction.startPageListen();

    // 处理单个Service
    await page.goto(`${config.href}/api/service/${config.serverName}/1.0.0.htm`, {
      timeout: 0, // 不限制超时时间
      waitUntil: [
        'load', // 等待 “load” 事件触发
        'domcontentloaded', // 等待 “domcontentloaded” 事件触发
        'networkidle0', // 在 500ms 内没有任何网络连接
        'networkidle2' // 在 500ms 内网络连接个数不超过 2 个
      ]
    });

    await indexAction.init();

  } catch(e){
    console.log('出错了: --------------------')
    console.log(e)
  } finally {
    // setTimeout(() => {
    //   await browser.close();
    // }, 10000);
  }

};
init();
