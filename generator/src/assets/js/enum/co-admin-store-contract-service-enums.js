export default {
	StoreHireRecodeStatusEnum: { MODIFY: { value: 'MODIFY', label: '修正' }, NORMAL: { value: 'NORMAL', label: '正常' } },
	StoreContractLargeCustomerEnum: { NO: { value: 'NO', label: '不是' }, YES: { value: 'YES', label: '是' } },
	StoreHireContractStatusEnum: {
		VALID: { value: 'VALID', label: '生效中' },
		INVALID: { value: 'INVALID', label: '已失效' },
	},
	StoreContractJoinPlanEnum: {
		A: { value: 'A', label: 'A方案' },
		B: { value: 'B', label: 'B方案' },
		C: { value: 'C', label: 'C方案' },
		D: { value: 'D', label: 'D方案' },
		E: { value: 'E', label: '特许加盟' },
		F: { value: 'F', label: '委托加盟' },
	},
	StoreContractPointsPlanEnum: {
		INCOME: { value: 'INCOME', label: '营业收入' },
		GROSS: { value: 'GROSS', label: '毛利额' },
	},
	TsStoresStoreTypeEnum: {
		TEQU_JOIN: { value: 'TEQU_JOIN', label: '特区加盟' },
		YUN_JOIN: { value: 'YUN_JOIN', label: '云加盟' },
		JOIN: { value: 'JOIN', label: '特许加盟' },
		TOUZI_JOIN: { value: 'TOUZI_JOIN', label: '投资加盟' },
		INNER_JOIN: { value: 'INNER_JOIN', label: '内加盟' },
		DIRECT: { value: 'DIRECT', label: '直营' },
	},
	StoreContractStatusEnum: {
		CANCEL: { value: 'CANCEL', label: '已作废' },
		INVALID: { value: 'INVALID', label: '已过期' },
		NORMAL: { value: 'NORMAL', label: '生效中' },
	},
}
