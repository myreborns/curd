
# 前要

目前的代码生成器基本上是基于数据库表格来生成对应的前后台代码，好处是前后台一次性搞定．但实际开发中，很多的业务是需要连表的，所以这种情况来说，根据接口直接生成前代码，就免除了与后台接字段的时间，也免除了很复制操作．

由此，我生出编写根据接口生成前端代码生成器的想法．
因为我司api文档有两种，前端又有vue2+element 和 vue3+ant+ts两种框架，由此，扩展出三种代码生成器．

# 思路

前端代码生成器，简单的说：

>　第一步，通过接口文档获取接口的入参＼出参，并将这入参\出参格式化成特定JSON

>　第二步，根据自己的项目，结合表格、表单组件，抽离出代码模板

>　第三步，通过第一二步的JSON、代码模板，生成vue、JS以及路由文件

# 简介

项目采用puppeteer对接口文档网页读取，获得接口链接、入参、出参等，并记录半生成特定格式的json．
项目中使用的是typescript，使用比较简单，可以用初学者入门



# 技术方案

获接口文档中接口的入参＼出参，我采用爬虫框架puppeteer来获取，根据获得的入参\出参，结合代码模板，生成实际放入工程的增删改查文件，做到增删改查基本不用再码代码．同时支持审核等表格操作

通过fs.writeFile来生成文件

# 说明

目前项目中只给了根据接口生成vue2+element的代码．代码是根据特定组件生成的．
因项目是为了配合公司的接口文档，生成公司的后台管理网站，定制化较高，所以此项目对于其他朋友来说，可做参考，也可能过修改模板来实现自己工程的代码生成．如其他交流，可以联系我．

  
>   表格、表单组件见项目中组件目录，这个需要全局引入．见components目录


另有生成vue3+typescript+ant的代码，暂未公布．

# 组件说明
后台管理系统大部分是由表格＋表单的页面，而表格＼表单要一个个去写，特别是还有自己的样式要求的情况，需要添加class之类的蛮麻烦，有时候一个拼写错误＼复制的时候没注意，就可能要找半天，看起来麻烦，本人更喜欢配置型的代码，所有站在高人的肩膀上，改出这个表格、表单组件．不喜勿喷．


# 配置说明

scale的配置

```js
// 适用于scale的配置
export const config = {
  href: 'http://xxx.cn/',
  serverName: 'AdminStoreContractService',
  pages: [{
    filePath: 'promotionNew',     // 列表＼新＼修页面所在路径
    chunkName: 'base', // chunkName 用于路由中 require.ensure 打包参数
    list: { // 列表
      fileName: 'pn-list', // 列表文件名字
      name: '列表', // 菜单名
      apiName: 'listStoreContract',  // 列表接口名
      exportFile: { // 导出 [非必填]
        apiUrl: '',
        downFileName: ''
      }
    },
    modify: { // (修改＼新增)  [非必填]
      fileName: 'modify',  // 页面名
      apiName: 'saveStoreContract',  // 接口名
      detailApi: 'getStoreContractById', // 查看详情接口名 (用于查看详情、编辑)
      hasEdit: true
    },
    opts: [ // 表格行操作
      {
        apiName: 'modifyStoreContractStatus', // 接口
        confrimMsg: '确认审核该记录', // 如果没有确认信息，则直接请求
        text: '审核'
      }
    ]
  }]
}
```

swagger api 的配置
```js

// 适用于swagger api 的配置
export const swaggerConfig = {
  href: 'http://xxx.com',
  serverName: 'mini-new-promotion-service',
  baseUrl: '/promotion-new/manager/',
  pages: [{
    filePath: 'promotionNew',     // 列表＼新＼修页面所在路径
    chunkName: 'base', // chunkName 用于路由中 require.ensure 打包参数
    list: { // 列表
      fileName: 'pn-list', // 列表文件名字
      name: '列表', // 菜单名
      apiName: 'getPromotionList',  // 列表接口名
      exportFile: { // 导出 [非必填]
        apiUrl: '',
        downFileName: ''
      }
    },
    modify: { // (修改＼新增)  [非必填]
      fileName: 'modify',  // 页面名
      apiName: 'savePromotion',  // 接口名
      detailApi: 'promotionDetail', // 查看详情接口名 (用于查看详情、编辑)
      hasEdit: true
    },
    opts: [ // 表格行操作
      {
        apiName: 'updPromotionConfigStatus', // 接口
        confrimMsg: '确认审核该记录', // 如果没有确认信息，则直接请求
        text: '审核'
      }
    ]
  }]
}
```



# 代码生成命令
　
目前本项目支持两种api文档，　第一种支持swagger api；第二种支持scale[这种用户比较少]

执行命令前，请在config.ts/swaggerConfig.ts中将配置修改好，如配置好需要生成的代码的网址


- swagger的代码命令
```
yarn run scr
```

- scale的代码命令
```
yarn run start
```

生成的文件：


![image.png](https://p9-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/eb06ad64946e46658da5f251b2cc6ce2~tplv-k3u1fbpfcp-watermark.image?)


![image.png](https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/914be480a99049e783b8d85a3b2dd928~tplv-k3u1fbpfcp-watermark.image?)
  
 
![image.png](https://p6-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/21088e9c8b514d379679a470011deb37~tplv-k3u1fbpfcp-watermark.image?)

废话不多说，上代码：
    
    
    https://gitee.com/myreborns/curd
    
    
生成器持续更新中，欢迎各位小伙伴交流，如果好用，给个赞．

# scale\swagger对比



|  | scale | swagger |
| --- | --- |--- |
| 枚举 | 同一个服务下的枚举都会放在同一个地方，是可以直接放入项目中使用的文件 |　自己根据后台的描述慢慢拆吧 |
| 出参＼入参获取 | 需要一层一层的获取 |　在页面打开的时，就有请求获取，直接获取请求的返回值就好 |


# 兼容各位自己的业务组件 [这里可以根据各位自己的实现情况来添加]

通常，我们会在项目中，抽离出一些业务组件，比如说图片组件(含上传，预览等)，员工(带搜索，用于选择员工)，这些组件如果没有全局引入，可以在　\components\co-form\components\co-form-item.vue　里面引入组件

而在 \src\util\tool.ts 里面，可以参照co-image-upload来添加各位自己的组件


# form-item 说明

```
{
  field: 'supplierName',
  label: '供应商名称:',
  component: 'el-input',
  componentProps: { // 组件属性
    placeholder: '请输入供应商名称',
    maxLength: 50
  },
  on: { // 组件事件
    blur: (e) => {
      console.log('-blur--->')
      // let s = this.$refs.basicFormFinanceRef.getFormData()
      if (this.$refs.basicFormFinanceRef) {
        this.$refs.basicFormFinanceRef.setFieldsValue({
          'bankName': e.srcElement.value
        })
      }
    }
  },
  dynamicRules: [ // 组件验证规则
    { required: true, message: '请输入供应商名称', trigger: 'blur' }
  ]
},
```


时间区间，后台用两个字段接收时, initFormData 用于赋默认值, fieldMapToTime， 用于格式化字段并将开始＼结束给对应的字段　

```
{
  showIndex: true, // 显示序号
  apiName: 'AdminSupplierQueryService.listCommonExportFileRecordQuery',
  listResultName: 'exportFileRecordList',
  initFormData: {
    dataTime: [(new Date().getTime() - 1000 * 60 * 60 * 24 * 30), new Date().getTime()]
    // createdBy: util.tdSessionStore.get('currentUser').id
  },
  beforeFetch: function (param) {
    param.createdBy = util.tdSessionStore.get('currentUser').id
    return param
  },
  immediate: false,
  searchForm: {
    baseColProps: { span: 8 },
    schemas: [
      {
        field: 'fileType',
        label: '单据名称:',
        component: 'el-select',
        componentProps: {
          placeholder: '请选择单据名称',
          options: util.getEnumValues(supplierEnums.ExportFileRecordTypeEnum, true)
        }
      },
      {
        field: 'dataTime',
        label: '导出日期:',
        component: 'el-date-picker',
        componentProps: {
          valueFormat: 'timestamp',
          type: 'daterange',
          'range-separator': '至',
          defaultTime: ['00:00:00', '23:59:59']
        }
      },
      {
        field: 'fileStatus',
        label: '导出状态:',
        component: 'el-select',
        componentProps: {
          options: util.getEnumValues(supplierEnums.ExportFileRecordStatusEnum, true)
        }
      }
    ],
    fieldMapToTime: [
      ['dataTime', ['createdAtStart', 'createdAtEnd']]
    ]
  },
  columns: [
    {
      prop: 'tExportFileRecord.fileType',
      label: '单据名称',
      formatter: (row, cloumn, value) => { return util.getEnumLabel(supplierEnums.ExportFileRecordTypeEnum, value) }
      // width: '110px'
    },
    {
      prop: 'tExportFileRecord.createdAt',
      label: '开始时间',
      formatter: (row, cloumn, value) => { return value ? util.formatDate(value, 'YYYY-MM-DD hh:mm:ss') : '--' }
    },
    {
      prop: 'tExportFileRecord.updatedAt',
      label: '结束时间',
      // width: '110px',
      formatter: (row, cloumn, value) => { return value ? util.formatDate(value, 'YYYY-MM-DD hh:mm:ss') : '--' }
    },
    {
      prop: 'tExportFileRecord.fileStatus',
      label: '导出状态',
      formatter: (row, cloumn, value) => { return util.getEnumLabel(supplierEnums.ExportFileRecordStatusEnum, value) }
    }
  ]
}
```
