import utilTool from "../../util/tool"
import util from '../common/util'
import { IJson } from "../../interface/type"
import { editTemp } from "../template/editTemp"
import { editDataTemp } from "../template/editDataTemp"
import { SwResult } from "../../store/swResult"

export async function createEditFile(pageConfig: IJson, sw: SwResult) {
  // 指定要创建的目录和文件名称 __dirname为执行当前js文件的目录
  const filePath = await utilTool.checkAndCreatePath(`/src/components/${pageConfig.filePath}`)
  const param = util.getResult(pageConfig?.modify?.apiName, sw)
  console.log('filePath: ', filePath, param)
  utilTool.generateFile(filePath + `/${pageConfig.modify.fileName}.data.js`, editDataTemp(param, pageConfig))
  utilTool.generateFile(filePath + `/${pageConfig.modify.fileName}.vue`, editTemp(pageConfig))
}
