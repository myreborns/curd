import utilTool from "../../util/tool"
import { IJson } from "../../interface/type"
import { routerTemp } from "../template/routerTemp"

export async function createRouterFile(pageConfig: IJson) {
  // 指定要创建的目录和文件名称 __dirname为执行当前js文件的目录
  const filePath = await utilTool.checkAndCreatePath(`/src/router/${pageConfig.filePath}`)
  utilTool.generateFile(filePath + `/${pageConfig.list.fileName}-router.js`, routerTemp(pageConfig))
}
