import { IJson } from "../../interface/type"
import utilTool from "../../util/tool"
import { config } from '../../config'

export function editTemp(pageConfig: IJson): string {
  const upServerName = utilTool.lineToUp(config.serverName)
  return `<template>
  <div class="c-container td-page ${pageConfig?.modify?.fileName}-page">
    <div class='c-add-content'>
      <co-basic-form ref='formRef' v-bind='formConfig'></co-basic-form>
      <div class='flex flex-end mt-20'>
        <!-- <el-button type="" @click.stop="doBack">返回列表</el-button> -->
        <el-button class="el-button--danger" @click.stop="save">保存</el-button>
      </div>
    </div>
  </div>
</template>
<script>
import { formSchemas } from './${pageConfig?.modify?.fileName}.data'
import api from '@/api/api'
// import util from '@/assets/js/co-util'
export default {
  data () {
    return {
      formConfig: {
        // formAttr: {
        //   'label-position': 'top'
        // },
        showActionButtonGroup: false,
        rowAttr: {
          gutter: 20
        },
        baseColProps: { span: 6 },
        schemas: formSchemas
      }
    }
  },
  created () {

  },
  mounted () {
    let id = this.$route.params.id
    if (id) {
      this.getDetail(id)
    }
  },
  methods: {
    // doBack () {
    //   this.$router.back(-1)
    // },
    getDetail (id) {
      api.${upServerName}.${pageConfig?.modify?.detailApi}({
        data: { id: id }
      }).then(res => {
        // 查看详情的接口返值的key需要与后台约定，如果不是约定，那么需要开发改一下
        this.$refs.formRef.setFieldsValue(res.data)
      })
    },
    save () {
      const formData = this.$refs.formRef.validate()
      api.${upServerName}.${pageConfig?.modify?.apiName}({
        data: { request: formData }
      }).then(res => {
        util.message({
          type: 'success',
          message: '保存成功'
        })
        this.$parent.removeTab()
      })
    }
  }
}
</script>
`
}
