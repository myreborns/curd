import { IEnum, IResAndReqFieldsMap } from "../../interface/type"
import util from "../common/util"

function formatArr(arr: IEnum[]): string {
  const strArr: string[] = []
  arr?.forEach(it => {
    strArr.push(`${it.value} = '${it.name}',
    `)
  })
  return strArr.join('')
}

function getEnumContent(json: IResAndReqFieldsMap) {
  const res = util.getAllEnums(json)
  console.log('---------------------枚举值---------------------------')
  console.log(res)
  const strArr = []
  for (const key of Object.keys(res)) {
    const item = res[key]
    strArr.push(`export enum ${key} {
      // ${item?.description ? item?.description : ''}
      ${formatArr(item.enums)}
    }
    `)
  }
  return strArr.join('')
}

export function enumTemp(json: IResAndReqFieldsMap) {
  return `${getEnumContent(json)}`
}
