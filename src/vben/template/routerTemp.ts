import utilTool from "../../util/tool"
import { IJson } from "../../interface/type"
import { vbenConfig } from '../../vbenConfig'

export function routerTemp(pageConfig: IJson) {
  const basePath = pageConfig.filePath ? pageConfig.filePath : vbenConfig.baseUrl.replace(/\//g, '')
  const fileName = pageConfig.list?.fileName ? pageConfig.list.fileName : 'index'

  return `import type { AppRouteModule } from '/@/router/types';
import { LAYOUT } from '/@/router/constant';

const ${utilTool.firstLower(utilTool.lineToUp(fileName))}: AppRouteModule = {
  path: '/${basePath}',
  name: '${utilTool.lineToUp(fileName)}',
  component: LAYOUT,
  meta: {
    icon: 'ion:menu-outline',
    title: '${pageConfig.list?.name}',
  },
  children: [
    {
      name: '${utilTool.lineToUp(fileName)}Management',
      path: 'index',
      component: () => import('/@/views/${vbenConfig.modelName}/${basePath}/${fileName}.vue'),
      meta: {
        title: '${pageConfig.list?.name}'
      }
    }
  ],
};

export default ${utilTool.firstLower(utilTool.lineToUp(fileName))};
  `
}
