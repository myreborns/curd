import utilTool from "../../util/tool"
import { IJson } from "../../interface/type"
import { routerTemp } from "../template/routerTemp"
import { menuTemp } from "../template/menuTemp"
import { vbenConfig } from '../../vbenConfig'

export async function createRouterFile(pageConfig: IJson) {
  if (!pageConfig?.list?.apiName) {
    return
  }
  // 指定要创建的目录和文件名称 __dirname为执行当前js文件的目录
  const fileName = pageConfig.filePath ? pageConfig.filePath : vbenConfig.baseUrl.replace(/\//g, '')
  const filePath = await utilTool.checkAndCreatePath(`/src/router/routes/modules/${vbenConfig.modelName}`)

  utilTool.generateFile(filePath + `/${fileName}.ts`, routerTemp(pageConfig))

  // 路由文件 menu
  const menuFilePath = await utilTool.checkAndCreatePath(`/src/router/menus/modules/${vbenConfig.modelName}`)
  utilTool.generateFile(menuFilePath + `/${fileName}.ts`, menuTemp(pageConfig))
}
