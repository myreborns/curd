import { IJson, IField } from "../../interface/type"
import utilTool from "../../util/tool"
import { config } from '../../config'

function getSchema(children: IField[], parentKey: string, arr: string[][], index: number) {
  const fArr: string[] = []
  if (parentKey) {
    fArr.unshift(`export const ${parentKey}Schemas = [`)
    parentKey = parentKey + '.'
    // fArr.push(``)
  } else {
    fArr.push(`export const formSchemas = [`)
  }
  console.log('-chidren--:', JSON.stringify(children))
  for (const item of children) {
    if (typeof item !== "object") {
      continue
    }
    // const currentKey = parentKey + key
    const currentKey = item.name
    if (item && item.children && item.children.length > 0) {
      getSchema(item.children, currentKey, arr, arr.length)
    } else if (item) {
      const upServerName = utilTool.firstLower(utilTool.lineToUp(config.serverName))
      utilTool.updateCompProp(item, item.name, upServerName)
      const descArr = utilTool.getDescription(item?.description || '')
      fArr.push(`
      {
        field: '${currentKey}',
        label: '${descArr[0]}:',${descArr[1]}
        component: '${item.component}',
        ${item.compProps}
      },`)
    }
  }
  fArr.push(`]
  `)
  arr[index] = fArr
}

// 根据请求参参数生成搜索条件配置
function getSchemas(json: IField[]): string {
  const cjson = JSON.parse(JSON.stringify(json))
  const arr: string[][] = [[]]
  getSchema(cjson, '', arr, 0)
  const res:string[] = []
  arr.forEach((item) => {
    res.push(item.join(''))
  })
  return res.join('')
}

export function editDataTemp(json: IField[], pageConfig: IJson) {
  console.log('开始读取数据，准备生成', pageConfig.modify.fileName + '.data.js')
  const upServerName = utilTool.lineToUp(config.serverName)
  const enumFileName: string = utilTool.upToLine(`co-${utilTool.firstLower(config.serverName)}Enum`)
  const lowServerName = utilTool.firstLower(upServerName)
  return `import ${lowServerName}Enum from '@/assets/js/enum/${enumFileName}'
import util from '@/assets/js/co-util'
  ${getSchemas(json)}
  `
}
