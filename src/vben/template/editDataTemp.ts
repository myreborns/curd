import utilTool from "../../util/tool"
import { vbenConfig } from '../../vbenConfig'
import { IJson, IResAndReqFieldsMap, IResAndReqFields, IField } from "../../interface/type"
import util from "../common/util"

// 修改的form
function getModifySchema(children: IField[], parentKey: string, arr: string[][], index: number) {
  const fArr: string[] = []
  if (parentKey) {
    fArr.unshift(`export const ${parentKey}Schemas = [`)
    parentKey = parentKey + '.'
    // fArr.push(``)
  } else {
    fArr.push(`export const formSchemas = [`)
  }
  console.log('-chidren--:', JSON.stringify(children))
  for (const item of children) {
    if (typeof item !== "object") {
      continue
    }
    // const currentKey = parentKey + key
    const currentKey = item.name
    if (item && item.children && item.children.length > 0) {
      getModifySchema(item.children, currentKey, arr, arr.length)
    } else if (item) {
      util.updateCompProp(item)
      const descArr = utilTool.getDescription(item?.description || '')
      if (item.name === 'id') {
        fArr.push(`
        {
          field: '${currentKey}',
          label: '${descArr[0]}:',${descArr[1]}
          component: '${item.component}',
          show: false,
          ${item.compProps}
        },`)
      } else {
        fArr.push(`
        {
          field: '${currentKey}',
          label: '${descArr[0]}:',${descArr[1]}
          component: '${item.component}',
          ${item.compProps}
        },`)
      }
    }
  }
  fArr.push(`]
  `)
  arr[index] = fArr
}

// 修改的form
function getModifySchemas(json: IResAndReqFields): string {
  if (!json?.request) {
    return ''
  }
  const cjson = JSON.parse(JSON.stringify(json.request))
  console.log('---------->cjson： ', JSON.stringify(json.request))
  const arr: string[][] = [[]]
  getModifySchema(cjson, '', arr, 0)
  const res: string[] = []
  arr.forEach((item) => {
    res.push(item.join(''))
  })
  return res.join('')
}

export function editDataTemp(apiPramAndRes: IResAndReqFieldsMap, pageConfig: IJson) {
  console.log('开始读取数据，准备生成', pageConfig.modify.fileName + '.data.js')
  console.log('json---------------------------------->', JSON.stringify(apiPramAndRes))
  // const upServerName = utilTool.lineToUp(vbenConfig.serverName)
  // const lowServerName = utilTool.firstLower(upServerName)
  const res = util.getAllEnums(apiPramAndRes)
  const basePath = pageConfig.filePath ? pageConfig.filePath : vbenConfig.baseUrl.replace(/\//g, '')
  const enumFileName = utilTool.firstLower(utilTool.lineToUp(`${basePath}Enum`))

  let modifyFormStr = ''
  if (pageConfig.modify?.apiName) {
    modifyFormStr = getModifySchemas(apiPramAndRes[pageConfig.modify.apiName])
  }

  return `import util from '@/assets/js/co-util'
import {
  ${Object.keys(res).join(', ')}
} from '/@/enums/${enumFileName}';
import { dateUtil } from '/@/utils/dateUtil';
import { enumsToOption, formatAmount } from '/@/utils/tools';
// import { h } from 'vue';
// import { Tag } from 'ant-design-vue'

${modifyFormStr}
  `
}
