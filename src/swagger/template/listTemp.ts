import { IJson } from "../../interface/type"
import utilTool from "../../util/tool"
import { swaggerConfig } from '../../swaggerConfig'

const funArr: string[]  = [] // 操作方法数组
const optHtmlArr: string[]  = [] // 操作方法html
function handleOpts(pageConfig: IJson, upServerName: string) {
  // 是否存在编辑
  if (pageConfig.modify) {
    if (pageConfig.modify?.apiName) {
      funArr.push(`add (row) {
        // 新增
        this.$router.push({name: '${pageConfig.modify.fileName}', params: { id: 0, type: 'add' }})
      },
    `)
    }
    if (pageConfig.modify.hasEdit) {
      optHtmlArr.push(`<el-button type="text" @click="edit(scope.row)">编辑</el-button>`)
      funArr.push(`edit (row) {
        // 编辑
        this.$router.push({name: '${pageConfig.modify.fileName}', params: { id: row.id, type: 'edit' }})
      },
    `)
    }
  }

  pageConfig.opts.forEach((it: IJson) => {
    optHtmlArr.push(`<el-button type="text" @click="${it.apiName}(scope.row)">${it.text}</el-button>`)
    if (it.confrimMsg) {
      funArr.push(`${it.apiName} (row) {
        util.confirm('${it.confrimMsg}', this.do${utilTool.firstUp(it.apiName)}, row)
      },
      do${utilTool.firstUp(it.apiName)} (row) {
        // 这里的参数需要开发者修改
        api.${upServerName}.${it.apiName}({
          data: { id: row.id }
        }).then(res => {
          this.$refs.tableRef.search()
          util.message({
            type: 'success',
            message: '操作成功'
          })
        })
      },
      `)
    } else {
      funArr.push(`${it.apiName} (row) {
        // 这里的参数需要开发者修改
        api.${upServerName}.${it.apiName}({
          data: { id: row.id }
        }).then(res => {
          this.$refs.tableRef.search()
          util.message({
            type: 'success',
            message: '操作成功'
          })
        })
      },
      `)
    }
  })
}

// 导出的方法
function getExportFileFun(pageConfig: IJson): string {
  if (!pageConfig.list?.exportFile?.apiUrl) {
    // 存在导出链接的时候，创建导出方法
    return ''
  }
  return `exportFile (param) {
    let request = JSON.parse(JSON.stringify(param))
    util.dealEmptyQueryCondition(request)
    api.crud.post({
      type: 'get',
      data: request,
      url: '${pageConfig.list.exportFile.apiUrl}',
      contentType: 'application/x-www-form-urlencoded',
      responseType: 'blob'
    }).then(res => {
      const date = util.getSpecDate()
      const fileName = date + '_${pageConfig.list.exportFile.downFileName}.xlsx'
      const url = URL.createObjectURL(res)
      util.downloadFile(url, fileName)
    })
  },`
}

function getOptColumn(pageConfig: IJson): string {
  if (pageConfig.opts && pageConfig.opts.length > 0) {
    const len = 60 + (pageConfig.opts.length * 60)
    return `
    <el-table-column label="操作" width="${len}" fixed="right">
        <template slot-scope="scope">
         ${optHtmlArr.join('')}
        </template>
      </el-table-column>
    `
  }
  return ''
}

// 数据模板
export function listTemp(json: IJson, pageConfig: IJson) {
  console.log('json: ', json)
  if (!pageConfig.list.fileName) {
    return ''
  }
  const upServerName = utilTool.lineToUp(swaggerConfig.serverName)
  // 根据规则生成操作按钮、方法
  handleOpts(pageConfig, upServerName)
  return `<!-- ${pageConfig.list.name} -->
<template>
  <div class="td-page">
    <co-table ref='tableRef' v-bind='tableConfig' ${pageConfig.list?.exportFile?.apiUrl ? "@exportFile='exportFile'" : ""}>
      ${ pageConfig?.modify?.apiName ? `<div slot='toolbar'>
        <el-button class="el-button--danger c-l-w-100-button" @click.stop="add">新建</el-button>
      </div>` : ``}
      ${getOptColumn(pageConfig)}
    </co-table>
  </div>
</template>

<script>
import api from '@/api/api'
import util from '@/assets/js/co-util'
import { tableConfig } from './${pageConfig.list.fileName}.data.js'
export default {
  name: '${pageConfig.list.fileName}',
  data () {
    return {
      tableConfig: Object.assign({
        apiName: '${upServerName}.${pageConfig.list.apiName}'
      }, tableConfig.call(this)),
    }
  },
  activated () {
    this.$refs.tableRef.search()
  },
  methods: {
    ${getExportFileFun(pageConfig)}
    ${funArr.join('')}
  }
}
</script>
  `
}
