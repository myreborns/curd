import fs from "fs"
import { config } from '../../config'
import utilTool from '../../util/tool'

export async function createEnumJosnFile(jsonData: JSON) {
  // 格式化json
  const josnText = 'export default ' + JSON.stringify(jsonData)
  // 指定要创建的目录和文件名称 __dirname为执行当前js文件的目录
  const filePath = await utilTool.checkAndCreatePath('/src/assets/js/enum')
  const fileName = utilTool.upToLine(`co${config.serverName}Enum.js`)
  const file = utilTool.resolvePath(filePath, fileName)
  // 写入文件
  fs.writeFile(file, josnText, (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log('文件创建成功~' + file);
    }
  })
}

