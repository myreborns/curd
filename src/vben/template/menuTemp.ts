import utilTool from "../../util/tool"
import { IJson } from "../../interface/type"
import { vbenConfig } from '../../vbenConfig'

export function menuTemp(pageConfig: IJson) {
  const basePath = pageConfig.filePath ? pageConfig.filePath : vbenConfig.baseUrl.replace(/\//g, '')
  const fileName = pageConfig.list?.fileName ? pageConfig.list.fileName : 'index'
  return `import type { MenuModule } from '/@/router/types';
const ${utilTool.firstLower(utilTool.lineToUp(pageConfig.list.fileName))}: MenuModule = {
  orderNo: 120,
  menu: {
    path: '/${basePath}',
    name: '${utilTool.lineToUp(fileName)}',
    tag: {
      dot: true,
      type: 'warn',
    },
    children: [
      {
        path: 'index',
        name: '${utilTool.lineToUp(fileName)}Management',
        tag: {
          dot: true,
          type: 'warn',
        }
      }
    ],
  },
};
export default ${utilTool.firstLower(utilTool.lineToUp(pageConfig.list.fileName))};
  `
}
