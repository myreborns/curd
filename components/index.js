import CoTable from './co-table/co-table'
import CoBasicForm from './co-form/co-basic-form'
const install = function (Vue) {
  Vue.component(
    'co-table', CoTable
  )
  Vue.component(
    'co-basic-form', CoBasicForm
  )
}
export default {
  install
}
