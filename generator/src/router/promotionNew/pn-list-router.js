const PnList = (resolve) => require.ensure([], () => resolve(require('@/components/promotionNew/pn-list')), 'base')
const Modify = (resolve) => require.ensure([], () => resolve(require('@/components/promotionNew/modify')), 'base')
export const pnListRouter = [
	{
		path: '/pn-list',
		name: 'PnList',
		component: PnList,
		meta: {
			keepAlive: true,
			pageName: '列表',
		},
	},
	{
		path: '/modify/:type/:id',
		name: 'Modify',
		component: Modify,
		meta: {
			keepAlive: false,
			pageName: {
				/*
				 * tab栏显示的名称 支持多值传输
				 * */
				add: '新建',
				details: '详情',
				edit: '编辑',
			},
		},
	},
]
