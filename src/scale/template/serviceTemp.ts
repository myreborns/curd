import { IMethod } from "../../interface/type"
function getServeList(list: IMethod[]): string {
  const interfaces: string[] = []
  for (const item of list) {
    const desc = item.description
    if (desc && desc.indexOf("\n") > 0 ) {
      interfaces.push(`'${item.name}', // ${desc}`);
    } else {
      interfaces.push(`'${item.name}', // ${desc}
      `);
    }
  }
  return interfaces.join('')
}

export function serviceTemp(list: IMethod[], serverName: string) {
  return `
    import { serviceFactory } from '../base'
    const defaultOptions = {
      version: '1.0.0',
      service: '${serverName}'
    }
    const interfaces = [
      ${getServeList(list)}
    ]
    const ${serverName} = serviceFactory(interfaces, defaultOptions)
    export default ${serverName}
  `
}