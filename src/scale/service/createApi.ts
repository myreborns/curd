import { config } from "../../config"
import utilTool from "../../util/tool"
import { IMethod } from "../../interface/type"
import { serviceTemp } from "../template/serviceTemp"

export async function createApiFile(list: IMethod[]) {
  // 指定要创建的目录和文件名称 __dirname为执行当前js文件的目录
  const filePath = await utilTool.checkAndCreatePath('/src/api/service')
  utilTool.generateFile(filePath + `/${config.serverName}.js`, serviceTemp(list, config.serverName))
}

