import puppeteer from "puppeteer";
import { IndexAction } from "./swagger/action/indexAction";
import { swaggerConfig } from './swaggerConfig'
// const chalk = require("chalk");
// const puppeteer = require("puppeteer");
// const server = require("./action/index.ts")

// const log = console.log;
const init = async () => {
  const browser = await puppeteer.launch({
    headless: false,   // 有浏览器界面启动
    slowMo: 200,       // 放慢浏览器执行速度，方便测试观察
    args: [            // 启动 Chrome 的参数，详见上文中的介绍
      "–no-sandbox",
      "--window-size=1280,960",
      '--no-sandbox',
      '--disable-setuid-sandbox',
      '--disable-dev-shm-usage',
      '--disable-accelerated-2d-canvas',
      '--disable-gpu',
    ],
    // executablePath: 'google-chrome'
  });
  const page = await browser.newPage();
  // log(chalk.green("服务正常启动"))
  try {
    const indexAction = new IndexAction(page);
    await indexAction.startPageListen();
    await page.goto(`${swaggerConfig.href}`, {
      timeout: 0, // 不限制超时时间
      waitUntil: "networkidle0",
    });
  } catch (e) {
    console.log('出错了: --------------------')
    console.log(e)
  } finally {
    setTimeout(() => {
      browser.close();
    }, 10000);
  }

};
init();
