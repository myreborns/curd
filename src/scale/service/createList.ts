import utilTool from "../../util/tool"
import { IJson } from "../../interface/type"
import { listDataTemp } from "../template/listDataTemp"
import { listTemp } from "../template/listTemp"
import { IResAndReqFields } from "../../interface/type"

export async function createListFile(pageConfig: IJson, resAndReq: IResAndReqFields) {
  // 指定要创建的目录和文件名称 __dirname为执行当前js文件的目录
  const filePath = await utilTool.checkAndCreatePath(`/src/components/${pageConfig.filePath}`)
  utilTool.generateFile(filePath + `/${pageConfig.list.fileName}.data.js`, listDataTemp(resAndReq, pageConfig))
  utilTool.generateFile(filePath + `/${pageConfig.list.fileName}.vue`, listTemp(resAndReq, pageConfig))
}
