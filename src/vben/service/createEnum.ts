// import fs from "fs"
import { vbenConfig } from '../../vbenConfig'
import utilTool from '../../util/tool'
import { IResAndReqFieldsMap, IJson } from "../../interface/type"
import { enumTemp } from "../template/enumTemp"

export async function createEnumJosnFile(apiPramAndResMap: IResAndReqFieldsMap, pageConfig: IJson) {
  // 格式化json
  // 指定要创建的目录和文件名称 __dirname为执行当前js文件的目录
  const filePath = await utilTool.checkAndCreatePath('/src/enums')
  const basePath = pageConfig.filePath ? pageConfig.filePath : vbenConfig.baseUrl.replace(/\//g, '')
  const fileName = utilTool.firstLower(utilTool.lineToUp(`${basePath}Enum.ts`))
  // const res = getAllEnums(apiPramAndResMap)
  utilTool.generateFile(filePath + `/${fileName}`, enumTemp(apiPramAndResMap))
}
