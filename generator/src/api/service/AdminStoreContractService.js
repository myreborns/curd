import { serviceFactory } from '../base'
const defaultOptions = {
	version: '1.0.0',
	service: 'AdminStoreContractService',
}
const interfaces = [
	'batchSaveStoreHire', //  批量保存门店租赁租金信息
	'batchSaveStoreHireContract', //  批量保存门店租赁合同
	'changeStoreHire', //  修正门店租赁租金信息
	'getStoreContractById', //  查询门店合同详情
	'getStoreHireById', //  查询门店租赁租金信息详情
	'getStoreHireByNormalId', //  查询门店租赁正常租金信息关联的修正信息
	'listStoreContract', //  合同信息列表查询
	'listStoreHire', //  查询门店租赁租金信息列表
	'listStoreHireContract', //  查询门店租赁合同信息列表
	'modifyStoreContractStatus', //  修改门店合同状态
	'saveStoreContract', //  新增/修改门店合同
	'saveStoreHire', //  新增/修改门店租赁租金信息
	'scrapStoreHireContract', //  废弃门店租赁合同
]
const AdminStoreContractService = serviceFactory(interfaces, defaultOptions)
export default AdminStoreContractService
