import utilTool from "../../util/tool"
import util from '../common/util'
import { IJson } from "../../interface/type"
import { listDataTemp } from "../template/listDataTemp"
import { listTemp } from "../template/listTemp"
import { SwResult } from "../../store/swResult"

export async function createListFile(pageConfig: IJson, sw: SwResult) {
  // 指定要创建的目录和文件名称 __dirname为执行当前js文件的目录
  const filePath = await utilTool.checkAndCreatePath(`/src/components/${pageConfig.filePath}`)
  const param = util.getResult(pageConfig?.list?.apiName, sw)
  console.log('filePath: ', filePath, param)
  console.log('pageConfig: ', pageConfig)
  utilTool.generateFile(filePath + `/${pageConfig.list.fileName}.data.js`, listDataTemp(param, pageConfig))
  utilTool.generateFile(filePath + `/${pageConfig.list.fileName}.vue`, listTemp(param, pageConfig))
}
